from imdb_utils import IMDbUtils

import argparse
from progress.bar import IncrementalBar


def get_hidden_themes(imdb_ids):
    film_keywords = []

    with IncrementalBar('Retrieving movie data', max=len(imdb_ids), suffix='%(percent).1f%% - %(eta)ds remaining', check_tty=False) as bar:
        for imdb_id in imdb_ids:
            movie_data = IMDbUtils.get_movie_keywords(imdb_id)

            if 'keywords' in movie_data:
                keywords = set(movie_data['keywords'])
                film_keywords.append(keywords)

            bar.next()

    hidden_themes = set.intersection(*film_keywords)

    return hidden_themes


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('imdb_ids', nargs="+", default=[])

    args = parser.parse_args()

    hidden_themes = get_hidden_themes(args.imdb_ids)

    print(hidden_themes)
