from wiki_pages import FilmsByCountry, FilmsByReference, FilmsByYear, HiddenThemes, KeywordScores, ViewingsCsv
from vcinema_utils import VCinemaUtils

import argparse
import json
from progress.bar import IncrementalBar


def update_wiki(token_id, token_secret, update_csv, pages):
    if update_csv:
        print("Updating CSV")
        ViewingsCsv.update_viewings_csv(token_id, token_secret)

    print("Getting viewings")
    viewings = VCinemaUtils.get_vcinema_viewings(token_id, token_secret)

    update_films_by_year = 'years' in pages
    update_films_by_country = 'countries' in pages
    update_film_references = 'references' in pages
    update_hidden_themes = 'themes' in pages
    update_keyword_scores = 'scores' in pages

    data_fields = []
    if update_films_by_year:
        data_fields.append("year")

    if update_films_by_country:
        data_fields.append("countries")

    if update_film_references or update_hidden_themes or update_keyword_scores:
        data_fields.append("keywords")

    viewing_count = len(viewings)
    with IncrementalBar('Retrieving movie data', max=viewing_count, suffix='%(percent).1f%% - %(eta)ds remaining', check_tty=False) as bar:
        VCinemaUtils.add_imdb_data_to_viewings(viewings, data_fields, bar)

    print("Processing viewing data")

    if update_films_by_year:
        films_by_year = FilmsByYear.get_films_by_year(viewings)
        FilmsByYear.update_page(token_id, token_secret, films_by_year)
    if update_films_by_country:
        films_by_country = FilmsByCountry.get_films_by_country(viewings)
        FilmsByCountry.update_page(token_id, token_secret, films_by_country)
    if update_film_references:
        films_by_reference = FilmsByReference.get_films_by_reference(viewings)
        FilmsByReference.update_page(token_id, token_secret, films_by_reference)
    if update_hidden_themes:
        hidden_themes = HiddenThemes.get_hidden_themes(viewings, token_id, token_secret)
        HiddenThemes.update_page(token_id, token_secret, hidden_themes)
    if update_keyword_scores:
        keyword_scores = KeywordScores.get_keyword_scores(viewings)
        KeywordScores.update_page(token_id, token_secret, keyword_scores)

    print("Done!")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Update wiki pages.')

    parser.add_argument('--pages', nargs="+", default=['years', 'countries', 'references', 'themes', 'scores'], required=False)
    parser.add_argument("--do_not_update_csv", action="store_true")

    args = parser.parse_args()

    with open('token.json') as json_file:
        token = json.load(json_file)

    update_wiki(token['token_id'], token['token_secret'], not args.do_not_update_csv, args.pages)
