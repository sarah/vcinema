from wiki_pages import FilmsByCountry
from vcinema_utils import VCinemaUtils

import argparse
from collections import OrderedDict
import imageio
from progress.bar import IncrementalBar
from pygifsicle import optimize
from PIL import Image, ImageFont, ImageDraw, ImageFont
import io


def generate_map_timelapse(token_id, token_secret, filename):
    print("Getting viewings")
    viewings = VCinemaUtils.get_vcinema_viewings(token_id, token_secret, combine_repeat_viewings=False)

    viewing_count = len(viewings)
    with IncrementalBar('Retrieving movie data', max=viewing_count, suffix='%(percent).1f%% - %(eta)ds remaining', check_tty=False) as bar:
        VCinemaUtils.add_imdb_data_to_viewings(viewings, ['countries'], bar)

    date_viewings = VCinemaUtils.filter_viewings(viewings, "date_watched")

    date_viewings = OrderedDict(sorted(date_viewings.items(), key=lambda t: t[0]))

    running_country_counts = {}
    print(len(date_viewings.keys()))

    with imageio.get_writer(filename, mode='I', duration=0.1) as writer:
        for date, viewings in date_viewings.items():
            date_viewings_countries = VCinemaUtils.filter_viewings(viewings, "countries")

            for country in date_viewings_countries:
                if country in running_country_counts.keys():
                    running_country_counts[country] += date_viewings_countries[country]
                else:
                    running_country_counts[country] = date_viewings_countries[country]

            map = FilmsByCountry.draw_map(running_country_counts, file_name="map-{}.svg".format(date))

            stream = io.BytesIO(map)
            img = Image.open(stream)

            map_editable = ImageDraw.Draw(img)

            # macos font path
            font = ImageFont.truetype("/System/Library/Fonts/Supplemental/Arial.ttf", 48)

            # image is 655 high
            map_editable.text((2, 605), "{}".format(date), (255, 64, 0), font=font)

            img_byte_arr = io.BytesIO()
            img.save(img_byte_arr, format='PNG')
            img_byte_arr = img_byte_arr.getvalue()

            image = imageio.imread(img_byte_arr)
            writer.append_data(image)

    print("optimizing")

    optimize(filename)

    print("done")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create timelapse gif of vcinema countries')
    parser.add_argument('token_id', help='API token ID.')
    parser.add_argument('token_secret', help='API token secret.')
    parser.add_argument('filename', help='Name of output gif')

    args = parser.parse_args()

    generate_map_timelapse(args.token_id, args.token_secret, args.filename)
