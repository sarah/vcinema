from collections import OrderedDict
import wikipedia

from bookstack import Bookstack
from vcinema_utils import VCinemaUtils

# Page ID of https://wiki.jacknet.io/books/vcinema/page/references
PAGE_ID = 62


def get_films_by_reference(viewings):
    films_by_reference = {}

    for viewing in viewings:
        if "keywords" in viewing.keys():
            for keyword in viewing["keywords"]:
                if keyword.startswith("reference-to-"):

                    for reference in films_by_reference:
                        if keyword in films_by_reference[reference]["keywords"]:
                            films_by_reference[reference]["films"].append(viewing)
                            break
                    else:
                        keyword = keyword[13:]

                        if keyword.startswith("a-"):
                            keyword = keyword[2:]

                        if keyword.endswith("-character"):
                            keyword = keyword[:-10]

                        referenced = keyword.replace("-", " ")

                        try:
                            searches = wikipedia.search(referenced, suggestion=False)
                            referenced_page = wikipedia.page(title=referenced, auto_suggest=False)

                            page_title = referenced_page.title
                            page_url = referenced_page.url

                        except wikipedia.DisambiguationError as e:
                            page_title = e.title
                            page_title = page_title[0].upper() + page_title[1:]
                            page_url = VCinemaUtils.generate_wikipedia_url(page_title)
                        except wikipedia.PageError as _:
                            if len(searches) > 0:
                                try:
                                    referenced_page = wikipedia.page(title=searches[0], auto_suggest=False)

                                    page_title = referenced_page.title
                                    page_url = referenced_page.url
                                except wikipedia.DisambiguationError as e:
                                    page_title = e.title
                                    page_title = page_title[0].upper() + page_title[1:]
                                    page_url = VCinemaUtils.generate_wikipedia_url(page_title)
                            else:
                                page_title = referenced.title()
                                page_url = None

                        if page_title in films_by_reference.keys():
                            films_by_reference[page_title]["keywords"].append(keyword)

                            if viewing not in films_by_reference[page_title]["films"]:
                                films_by_reference[page_title]["films"].append(viewing)

                        else:
                            films_by_reference[page_title] = {"url": page_url,
                                                              "keywords": [keyword],
                                                              "films": [viewing]}

    return films_by_reference


def update_page(token_id, token_secret, films_by_reference_keyword):
    page = build_page(films_by_reference_keyword)
    Bookstack.update_page(VCinemaUtils.JACKNET_WIKI_URL, token_id, token_secret, PAGE_ID, markdown=page)


def build_page(films_by_reference):
    films_by_reference = OrderedDict(sorted(films_by_reference.items(), key=lambda t: t[0]))

    table = "| Referenced | Films |\n| - | - |"

    for reference, referenced in films_by_reference.items():
        table += "\n"

        row_data = []

        reference_url = referenced["url"]
        referenced_films = referenced["films"]

        if reference_url is None:
            row_data.append(reference)
        else:
            row_data.append(VCinemaUtils.generate_markdown_link(reference, reference_url))
        row_data.append(VCinemaUtils.get_film_list(referenced_films))

        table += " | ".join(row_data)

    return table
