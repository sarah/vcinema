from collections import OrderedDict

from bookstack import Bookstack
from vcinema_utils import VCinemaUtils

# Page ID of https://wiki.jacknet.io/books/vcinema/page/films-by-reference
PAGE_ID = 63


def get_hidden_themes(viewings, token_id, token_secret):
    # Bit horrible to need to request this again, but it affects the order of the result table
    viewings_ungrouped = VCinemaUtils.get_vcinema_viewings(token_id, token_secret, combine_repeat_viewings=False)

    # Copy keywords from grouped viewings to ungrouped viewings
    for viewing_ungrouped in viewings_ungrouped:
        for viewing in viewings:
            if viewing['imdb_id'] == viewing_ungrouped['imdb_id']:
                if 'keywords' in viewing:
                    viewing_ungrouped['keywords'] = viewing['keywords']
                    break

    viewings_filtered_watch_date = VCinemaUtils.filter_viewings(viewings_ungrouped, "date_watched")

    for date, viewings in viewings_filtered_watch_date.items():
        viewing_dict = {"viewings": viewings}

        viewings_filtered_watch_date[date] = viewing_dict

    # Add hidden themes
    for date, data in viewings_filtered_watch_date.items():
        keyword_counts = {}

        if len(data['viewings']) > 1:
            for viewing in data['viewings']:
                if 'keywords' in viewing:
                    for keyword in viewing['keywords']:
                        if keyword in keyword_counts.keys():
                            keyword_counts[keyword] += 1
                        else:
                            keyword_counts[keyword] = 1

            keyword_counts = {k: v for k, v in sorted(keyword_counts.items(), key=lambda item: item[1], reverse=True)}
            hidden_themes = {}

            for keyword in keyword_counts:
                rating = float(keyword_counts[keyword]) / float(len(data['viewings']))
                if rating > 0.5:
                    hidden_themes[keyword] = rating

            viewings_filtered_watch_date[date]['hidden_themes'] = hidden_themes

    return viewings_filtered_watch_date


def update_page(token_id, token_secret, hidden_themes):
    page = build_page(hidden_themes)
    Bookstack.update_page(VCinemaUtils.JACKNET_WIKI_URL, token_id, token_secret, PAGE_ID, markdown=page)


def build_page(hidden_themes):
    hidden_themes = OrderedDict(sorted(hidden_themes.items(), key=lambda t: t[0]))

    table = "| Date | Films | Hidden Themes |\n| - | - | - |"

    for date, data in hidden_themes.items():
        table += "\n"

        row_data = []
        row_data.append(str(date))
        row_data.append(VCinemaUtils.get_film_list(data['viewings']))
        if 'hidden_themes' in data and data['hidden_themes'] != {}:
            hidden_theme_labels = []

            for hidden_theme in sorted(data['hidden_themes'].keys()):
                if data['hidden_themes'][hidden_theme] == 1:
                    hidden_theme_labels.append(hidden_theme)
                else:
                    hidden_theme_labels.append("<i>{} ({}%)</i>".format(hidden_theme, round(data['hidden_themes'][hidden_theme] * 100)))

            row_data.append("<br>".join(hidden_theme_labels))
        else:
            row_data.append("N/A")

        table += " | ".join(row_data)

    return table
