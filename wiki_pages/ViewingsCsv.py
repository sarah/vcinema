from bs4 import BeautifulSoup

from bookstack import Bookstack
from vcinema_utils import VCinemaUtils

# Page ID of https://wiki.jacknet.io/books/vcinema/page/csv
PAGE_ID = 11


def update_viewings_csv(token_id, token_secret):
    print("Retrieving viewings page")
    html_page = Bookstack.get_page_html(VCinemaUtils.JACKNET_WIKI_URL, token_id, token_secret, PAGE_ID)

    soup = BeautifulSoup(html_page, 'html.parser')
    csv_data = soup.find("code").text.strip().encode('utf-8')

    existing_attachment_id = VCinemaUtils.get_viewings_csv_attachment_id(token_id, token_secret)
    print("Updating file")
    Bookstack.update_attachment(VCinemaUtils.JACKNET_WIKI_URL, token_id, token_secret, existing_attachment_id, "vcinema.csv", csv_data, PAGE_ID)
    print("File updated")
