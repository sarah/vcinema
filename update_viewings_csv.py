import json

from wiki_pages import ViewingsCsv


def update_viewings_csv(token_id, token_secret):
    print("Updating CSV")
    ViewingsCsv.update_viewings_csv(token_id, token_secret)
    print("Done!")


if __name__ == '__main__':
    with open('token.json') as json_file:
        token = json.load(json_file)

    update_viewings_csv(token['token_id'], token['token_secret'])
